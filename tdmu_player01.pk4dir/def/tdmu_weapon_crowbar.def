model viewmodel_crowbar
{
	mesh							models/md5/weapons/crowbar/crowbar.md5mesh
	offset							(-4 0 -14)

	anim raise						models/md5/weapons/crowbar/select.md5anim
	{
		frame 1						sound_weapon sword_unsheath
	}
	anim idle						models/md5/weapons/crowbar/idle.md5anim
	anim putaway					models/md5/weapons/crowbar/holster.md5anim 
	{
		frame 30					sound_weapon sword_sheath
	}
	anim attack_LR					models/md5/weapons/crowbar/attack_lr.md5anim
	{
		frame 10					melee_hold
		frame 12					sound snd_swing
		frame 12					melee_attack_start meleeweap_r slash_lr
		frame 20					melee_attack_stop meleeweap_r
	}
	anim attack_RL					models/md5/weapons/crowbar/attack_rl.md5anim
	{
		frame 10					melee_hold
		frame 12					sound snd_swing
		frame 12					melee_attack_start meleeweap_r slash_rl
		frame 20					melee_attack_stop meleeweap_r
	}
	anim attack_Over				models/md5/weapons/crowbar/attack_overhead.md5anim
	{
		frame 10					melee_hold
		frame 12					sound snd_swing
		frame 12					melee_attack_start meleeweap_r overhead
		frame 20					melee_attack_stop meleeweap_r
	}
	anim attack_Thrust				models/md5/weapons/crowbar/attack_thrust.md5anim
	{
		frame 10					melee_hold
		frame 12					sound snd_swing
		frame 12					melee_attack_start meleeweap_r thrust
		frame 20					melee_attack_stop meleeweap_r
	}
	anim parry_LR					models/md5/weapons/crowbar/parry_lr.md5anim
	{
		frame 8						melee_parry_start meleeweap_r right
		frame 10					melee_hold
		frame 11					melee_parry_stop meleeweap_r
	}
	anim parry_RL					models/md5/weapons/crowbar/parry_rl.md5anim
	{
		frame 8						melee_parry_start meleeweap_r left
		frame 10					melee_hold
		frame 11					melee_parry_stop meleeweap_r
	}
	anim parry_Over					models/md5/weapons/crowbar/parry_r.md5anim
	{
		frame 10					melee_parry_start meleeweap_r overhead
		frame 10					melee_hold
		frame 12					melee_parry_stop meleeweap_r
	}
	anim parry_Thrust				models/md5/weapons/crowbar/parry_thrust.md5anim
	{
		frame 8						melee_parry_start meleeweap_r thrust
		frame 10					melee_hold
		frame 11					melee_parry_stop meleeweap_r
	}
	anim cancel_LR			models/md5/weapons/crowbar/cancel_lr.md5anim
	{
		frame 1 					melee_attack_stop meleeweap_r
	}
	anim cancel_RL			models/md5/weapons/crowbar/cancel_rl.md5anim
	{
		frame 1 					melee_attack_stop meleeweap_r
	}
	anim cancel_Over			models/md5/weapons/crowbar/cancel_overhead.md5anim
	{
		frame 1						melee_attack_stop meleeweap_r
	}
	anim cancel_Thrust		models/md5/weapons/crowbar/cancel_thrust.md5anim
	{
		frame 1						melee_attack_stop meleeweap_r
	}
}

entityDef melee_crowbar
{
	"inherit"						"atdm:melee_base"

	"damage"						"30"
	"kickDir"						"0 0 0" // Customized by derivate entities

	"impact_damage_effect"			"1"
	"mtr_blob"						"genericDamage"
	"blob_time"						"300"
	"blob_size"						"400"
	"blob_offset_x"					"400"

	"knockback"						"10"
	"push"							"3200"
	"kick_time"						"400"
	"kick_amplitude"				"1"
	"dv_time"						"100"
	"gib"							"0"

	"damage_mult_flesh"				"1.0"
	"damage_mult_undeadflesh"		"0.25"
	"damage_mult_undeadbone"		"0.1"
	"damage_mult_cloth"				"1.0"
	"damage_mult_armor_leath"		"0.75"
	"damage_mult_armor_chain"		"0.5"
	"damage_mult_armor_plate"		"0"

	"smoke_wound_flesh"				"burstysquirt.prt"
	"smoke_wound_cloth"				"burstysquirt.prt"
	"smoke_wound_armor_leather"		"burstysquirt.prt"
	"smoke_wound_armor_plate"		"chainsawstrike.prt"
	"smoke_wound_metal"				"chainsawstrike.prt"
	"smoke_strike_metal"			"chainsawstrike.prt"
	"smoke_strike_armor_plate"		"chainsawstrike.prt"
	"smoke_chance_metal"			"0.4"
	"smoke_chance_armor_plate"		"0.5"

	"mtr_wound_flesh"				"textures/decals/hurt02"
	"mtr_wound_cloth"				"textures/decals/hurt02"
	"mtr_wound_armor_leath"			"textures/decals/hurt02"
	"mtr_wound_straw"				"textures/decals/hurt02"
	"mtr_killed_splat1"				"textures/darkmod/decals/blood/blood01"
	"mtr_killed_splat2"				"textures/darkmod/decals/blood/blood02"
	"mtr_killed_splat3"				"textures/darkmod/decals/blood/blood03"
	"mtr_killed_splat4"				"textures/darkmod/decals/blood/blood04"
	"mtr_killed_splat5"				"textures/darkmod/decals/blood/blood05"
	"mtr_killed_splat6"				"textures/darkmod/decals/blood/blood06"
	"mtr_killed_splat7"				"textures/darkmod/decals/blood/blood07"
	"mtr_killed_splat8"				"textures/darkmod/decals/blood/blood08"
	"mtr_killed_splat9"				"textures/darkmod/decals/blood/blood09"

	"default_surface_inanimate"		"metal"
	"default_surface_actor"			"flesh"

	"snd_hit"						"weapon_hit_default"
	"snd_metal"						"sword_hit_metal"
	"snd_ricochet"					"sword_hit_wood"
	"snd_flesh"						"sword_hit_flesh"
	"snd_undeadflesh"				"sword_hit_chain"
	"snd_stone"						"sword_hit_metal"
	"snd_wood"						"sword_hit_wood"
	"snd_cardboard"					"blackjack_hit_wood"
	"snd_glass"						"glass_bullethole"
	"snd_liquid"					"melee_hit_water"
	"snd_plastic"					"sword_hit_wood"
	"snd_armor_plate"				"sword_hit_armor_plate"
	"snd_armor_chain"				"sword_hit_chain"
	"snd_armor_leath"				"sword_hit_chain"
	"snd_cloth"						"sword_hit_flesh"	

	"sprS_metal"					"sword_hit_hard"
	"sprS_armor_plate"				"sword_hit_hard"
	"sprS_ricochet"					"sword_hit_hard"
	"sprS_stone"					"sword_hit_hard"
	"sprS_wood"						"sword_hit_hard"
	"sprS_glass"					"sword_hit_hard"
	"sprS_flesh"					"sword_hit_flesh"
	"sprS_cloth"					"sword_hit_flesh"
	"sprS_armor_chain"				"sword_hit_flesh"
	"sprS_armor_leath"				"sword_hit_flesh"
}

entityDef melee_crowbar_rl
{
	"inherit"						"melee_crowbar"
	"kickDir"						"0 -1 0"
}

entityDef melee_crowbar_lr
{
	"inherit"						"melee_crowbar"
	"kickDir"						"0 -1 0"
}

entityDef melee_crowbar_overhead
{
	"inherit"						"melee_crowbar"
	"kickDir"						"0 0 -1"
}

entityDef melee_crowbar_thrust
{
	"inherit"						"melee_crowbar"
	"kickDir"						"-1 0 0"
}

entityDef atdmu:moveable_weapon_crowbar
{
	"inherit"						"atdmu:moveable_weapon_melee"
	"model"							"models/md5/weapons/crowbar/crowbar.lwo"

	"mass"							"12"
}

entityDef atdmu:weapon_crowbar
{
	"inherit"						"atdm:weapon_base"

	"editor_usage"					"The Crowbar"
	"editor_displayFolder"			"Weapons/Melee"

	"inv_weapon_name"				"crowbar"
	"inv_name"						"Crowbar"
	"inv_icon"						"tdmu_hud_icon_weapon_crowbar"

	"spawnclass"					"idMoveable"
	"model"							"models/md5/weapons/crowbar/crowbar.lwo"
	"model_view"					"viewmodel_crowbar"

	"weapon_scriptobject"			"weapon_shortsword"

	"frobable"						"1"
	"is_weapon_melee"				"1"
	"inv_lgmodifier"				"2"
	"ammo_required"					"0"
	"impact_damage_effect"			"1"

	"def_melee"						"melee_crowbar_rl"
	"def_attach"					"atdmu:attachment_crowbar"
	"name_attach"					"meleeweap_r"

	"snd_acquire"					"tool_pickup"
	"snd_swing"						"sword_swing"

	"anim_rate_parry_LR"			"2.5"
	"anim_rate_parry_RL"			"2.5"
	"anim_rate_parry_Over"			"2.5"
	"anim_rate_parry_Thrust"		"2.5"

	"anim_rate_attack_LR"			"1.5"
	"anim_rate_attack_RL"			"1.5"
	"anim_rate_attack_Over"			"1.5"
	"anim_rate_attack_Thrust"		"1.5"

	"anim_rate_cancel_attack_LR"	"1.25"
	"anim_rate_cancel_attack_RL"	"1.25"
	"anim_rate_cancel_attack_Over"	"1.25"
	"anim_rate_cancel_attack_Thrust"	"1.25"

	"anim_rate_raise"				"1.5"
}

entityDef atdmu:attachment_crowbar
{
	"inherit"						"atdmu:moveable_weapon_crowbar"
	"spawnclass"					"CMeleeWeapon"
	"joint"							"Melee"

	"angles"						"0 0 0"
	"origin"						"0 0 0"

	"hide"							"1"
	"solid"							"0"

	"clipmodel_contents"			"0"
	"frobable"						"0"
	"neverShow"						"1"
	"noshadows"						"1"

	"stop_mass"						"8"

	"att_type_overhead"				"0"
	"att_world_collide_overhead"	"1"
	"def_damage_overhead"			"melee_crowbar_overhead"
	"att_mod_cm_overhead"			"1"
	"att_cm_maxs_overhead"			"1 20 1"
	"att_cm_mins_overhead"			"-1 5 -1"

	"att_type_slash_lr"				"1"
	"att_world_collide_slash_lr"	"1"
	"def_damage_slash_lr"			"melee_crowbar_lr"
	"att_mod_cm_slash_lr"			"1"
	"att_cm_maxs_slash_lr"			"1 20 1"
	"att_cm_mins_slash_lr"			"-1 5 -1"

	"att_type_slash_rl"				"2"
	"att_world_collide_slash_rl"	"1"
	"def_damage_slash_rl"			"melee_crowbar_rl"
	"att_mod_cm_slash_rl"			"1"
	"att_cm_maxs_slash_rl"			"1 20 1"
	"att_cm_mins_slash_rl"			"-1 5 -1"

	"att_type_thrust"				"3"
	"att_world_collide_thrust"		"1"
	"def_damage_thrust"				"melee_crowbar_thrust"
	"att_mod_cm_thrust"				"1"

	"att_cm_maxs_thrust"			"-1 20 -2"
	"att_cm_mins_thrust"			"-2 15 -4"

	"att_failsafe_trace_thrust"		"1"
	"att_failsafe_trace_start_thrust"	"0 -20 0"
	"att_failsafe_trace_end_thrust"	"0 42 0"

	"par_type_overhead"				"0"
	"par_mod_cm_overhead"			"1"
	"par_cm_maxs_overhead"			"20 40 20"
	"par_cm_mins_overhead"			"-20 0 -20"

	"par_type_left"					"2"
	"par_mod_cm_left"				"1"
	"par_cm_maxs_left"				"20 40 20"
	"par_cm_mins_left"				"-20 -35 -20"

	"par_type_right"				"1"
	"par_mod_cm_right"				"1"
	"par_cm_maxs_right"				"20 40 20"
	"par_cm_mins_right"				"-20 -35 -20"

	"par_type_thrust"				"3"
	"par_mod_cm_thrust"				"1"
	"par_cm_maxs_thrust"			"25 50 75"
	"par_cm_mins_thrust"			"-60 -50 -40"
	
	"par_cm_yaw_only_thrust"		"1"
}
