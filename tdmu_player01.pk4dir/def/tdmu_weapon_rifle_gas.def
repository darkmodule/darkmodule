/***********************************************************************
This entityDef contains the gas rifle.
***********************************************************************/

model viewmodel_rifle_gas
{
	mesh							models/md5/weapons/rifle/rifle_gas.md5mesh
	offset							(2 0 -16)

	anim raise 						models/md5/weapons/rifle/select.md5anim
	{
		frame 15					sound_weapon rifle_raise
	}
	anim idle 						models/md5/weapons/rifle/idle.md5anim
	anim lower	 					models/md5/weapons/rifle/holster.md5anim
	{
		frame 5						sound_weapon rifle_lower
	}
	anim attack_start				models/md5/weapons/rifle/draw.md5anim
	{
		frame 30					sound_weapon rifle_charge_slow // 3 or 4 attached parts
	}
	anim attack 					models/md5/weapons/rifle/fire.md5anim
	{
		frame 1						sound rifle_fire_gas
	}
	anim tired		 				models/md5/weapons/rifle/tired.md5anim
	anim cancel		 				models/md5/weapons/rifle/cancel.md5anim
}

entityDef atdmu:weapon_rifle_gas
{
	"inherit"						"atdmu:weapon_rifle"

	"editor_usage"					"The Gas Rifle"

	"spawnclass"					"idMoveable"
	"model"							"models/md5/weapons/rifle/rifle_gas.lwo"
	"model_view"					"viewmodel_rifle_gas"

	"def_projectile"				"atdmu:projectile_plasma_gas"
	"def_ammo"						"atdmu:ammo_cell_gas"
	"def_attach"					"atdmu:attachment_plasma_gas"

	"inv_name"						"Gas Rifle"
	"inv_icon"						"tdmu_hud_icon_weapon_rifle_gas"
	"inv_weapon_name"				"rifle_gas" // Corresponding ammo

	"anim_rate_raise"				"1.5"
	"anim_rate_idle"				"0.5"
	"anim_rate_lower"				"1.5"
	"anim_rate_attack_start"		"0.75"
	"anim_rate_attack"				"1.0"
	"anim_rate_tired"				"1.0"
	"anim_rate_cancel"				"1.0"
}

entityDef atdmu:ammo_cell_gas
{
	"inherit"						"atdm:ammo_arrow"

	"editor_usage"					"Gas Rifle Ammunition"
	"editor_displayFolder"			"Weapons/Rifles"

	"model"							"models/md5/weapons/rifle/cell_gas.lwo"

	"inv_name"						"Gas Rifle Cell"
	"inv_weapon_name"				"rifle_gas" // Corresponding weapon
	"inv_ammo_amount"				"1" // Amount of ammo received upon pickup

	"clipmodel_contents"			"1056" // MOVEABLE_CLIP|CORPSE
	"removeWithMaster"				"0"
}

entityDef atdmu:attachment_plasma_gas
{
	"inherit"						"atdmu:attach_plasma"
}

entityDef atdmu:projectile_plasma_gas
{
	"inherit"						"atdmu:projectile_plasma"

	"model"							"models/md5/weapons/rifle/plasma_gas.lwo"

	"has_result"					"1"
	"def_result"					"atdmu:result_plasma_gas"
	"def_damage"					"atdm:damage_none"
	"allow_result_in_water"			"0"

	"detonate_on_water"				"1"
	"def_result_water"				"atdmu:result_plasma_gas"
	"def_damage_water"				"atdm:damage_none"
	"no_water_splash_damage"		"1"
}

entityDef atdmu:result_plasma_gas
{
	"inherit"						"atdmu:result_plasma"

	"model_dud"						"gas_cloud.prt"

	"snd_dud"						"arrow_gas_explode"
	"sprS_dud"						"blackjack_hit_soft"    

	"copy_bind"						"0"

	"remove_delay"					"10"

	"sr_type_1"						"20" // STIM_GAS
	"sr_radius_1"					"20"
	"sr_radius_final_1"				"80"
	"sr_state_1"					"1"
	"sr_falloffexponent_1"			"1"
	"sr_duration_1"					"8000" // ms
	"sr_time_interval_1"			"300" // ms
}
