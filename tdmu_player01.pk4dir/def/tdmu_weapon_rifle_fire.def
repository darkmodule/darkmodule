/***********************************************************************
This entityDef contains the fire rifle.
***********************************************************************/

model viewmodel_rifle_fire
{
	mesh							models/md5/weapons/rifle/rifle_fire.md5mesh
	offset							(2 0 -16)

	anim raise 						models/md5/weapons/rifle/select.md5anim
	{
		frame 15					sound_weapon rifle_raise
	}
	anim idle 						models/md5/weapons/rifle/idle.md5anim
	anim lower	 					models/md5/weapons/rifle/holster.md5anim
	{
		frame 5						sound_weapon rifle_lower
	}
	anim attack_start				models/md5/weapons/rifle/draw.md5anim
	{
		frame 30					sound_weapon rifle_charge_slow // 3 or 4 attached parts
	}
	anim attack 					models/md5/weapons/rifle/fire.md5anim
	{
		frame 1						sound rifle_fire_fire
	}
	anim tired		 				models/md5/weapons/rifle/tired.md5anim
	anim cancel		 				models/md5/weapons/rifle/cancel.md5anim
}

entityDef atdmu:weapon_rifle_fire
{
	"inherit"						"atdmu:weapon_rifle"

	"editor_usage"					"The Fire Rifle"

	"spawnclass"					"idMoveable"
	"model"							"models/md5/weapons/rifle/rifle_fire.lwo"
	"model_view"					"viewmodel_rifle_fire"

	"def_projectile"				"atdmu:projectile_plasma_fire"
	"def_ammo"						"atdmu:ammo_cell_fire"
	"def_attach"					"atdmu:attachment_plasma_fire"

	"inv_name"						"Fire Rifle"
	"inv_icon"						"tdmu_hud_icon_weapon_rifle_fire"
	"inv_weapon_name"				"rifle_fire" // Corresponding ammo

	"anim_rate_raise"				"1.5"
	"anim_rate_idle"				"0.5"
	"anim_rate_lower"				"1.5"
	"anim_rate_attack_start"		"0.75"
	"anim_rate_attack"				"1.0"
	"anim_rate_tired"				"1.0"
	"anim_rate_cancel"				"1.0"
}

entityDef atdmu:ammo_cell_fire
{
	"inherit"						"atdm:ammo_arrow"

	"editor_usage"					"Fire Rifle Ammunition"
	"editor_displayFolder"			"Weapons/Rifles"

	"model"							"models/md5/weapons/rifle/cell_fire.lwo"

	"inv_name"						"Fire Rifle Cell"
	"inv_weapon_name"				"rifle_fire" // Corresponding weapon
	"inv_ammo_amount"				"1" // Amount of ammo received upon pickup

	"clipmodel_contents"			"1056" // MOVEABLE_CLIP|CORPSE
	"removeWithMaster"				"0"
}

entityDef atdmu:attachment_plasma_fire
{
	"inherit"						"atdmu:attach_plasma"
}

entityDef atdmu:projectile_plasma_fire
{
	"inherit"						"atdmu:projectile_plasma"

	"model"							"models/md5/weapons/rifle/plasma_fire.lwo"

	"impact_gib"					"1" // Gibs characters

	"has_result"					"1"
	"def_result"					"atdmu:result_plasma_fire"
	"def_damage"					"atdmu:damage_plasma_fire_direct"
	"def_splash_damage"				"atdmu:damage_plasma_fire_splash"

	"mtr_detonate"					"textures/decals/ballburn01"
	"decal_size"					"75"

	"detonate_on_water"				"1"
	"no_water_splash_damage"		"1"
	"def_result_water"				"atdm:result_plasma_fire_water"
	"def_damage_water"				"atdm:damage_none"
}

entityDef atdmu:result_plasma_fire
{
	"inherit"						"atdmu:result_plasma"

	"model_dud"						"tdm_firearrow_explosion.prt"
	"dud_align_surface"				"1"

	"snd_dud"						"arrow_fire_hit"
	"sprS_dud"						"arrow_fire_hit"

	"copy_bind"						"0"

	"remove_delay"					"4"

	"sr_type_1"						"1" // STIM_FIRE
	"sr_radius_1"					"10"
	"sr_radius_final_1"				"80"
	"sr_falloffexponent_1"			"1"
	"sr_duration_1"					"250" // ms
	"sr_time_interval_1"			"50" // ms
}

entityDef atdmu:result_plasma_fire_water
{
	"inherit"						"atdmu:result_plasma"

	"model_dud"						"tdm_smoke_torchout.prt"
	"dud_align_surface"				"1"

	"snd_dud"						"machine_steam01"

	"copy_bind"						"0"

	"remove_delay"					"1"
}

entityDef atdmu:damage_plasma_fire_direct {
	"inherit"						"atdm:damage_base"

	"damage"						"400"
	"radius"						"50"
	"push"							"600"
	"gib"							"1"

	"mtr_wound_flesh"				"textures/decals/hurt02"
	"mtr_splat_flesh"				""
	"smoke_wound_flesh"				"bloodwound.prt"
}

entityDef atdmu:damage_plasma_fire_splash
{
	"inherit"                   "atdm:damage_base"

	"damage"					"40"
	"radius"					"200"
	"push"						"200"
	"knockback"					"30"
	"gib"						"1"

	"attackerDamageScale"		"1.0"
	"attackerPushScale"			"1.0"
}
