model viewmodel_policebaton
{
	mesh							models/md5/weapons/policebaton/policebaton.md5mesh
	offset							(-4 0 -14)

	anim raise						models/md5/weapons/policebaton/select.md5anim
	{
		frame 1						sound_weapon blackjack_unsheath
	}
	anim idle 						models/md5/weapons/policebaton/idle.md5anim
	anim putaway 					models/md5/weapons/policebaton/holster.md5anim
	{
		frame 1						sound_weapon blackjack_sheath
	}
	anim hit_start 					models/md5/weapons/policebaton/attack_start.md5anim
	anim hit 						models/md5/weapons/policebaton/attack.md5anim
	{
		frame 5						sound snd_swing
		frame 5						melee_attack_start meleeweap_r overhead
		frame 10					melee_attack_stop meleeweap_r
	}
}

entityDef melee_policebaton
{
	"inherit"						"atdm:damage_base"
	"damage"						"2"
	"knockout"						"1"
	"kickDir"						"0 0 0" // Customized by derivate entities

	"impact_damage_effect"			"1"
	"mtr_blob"						"genericDamage"
	"blob_time"						"300"
	"blob_size"						"400"
	"blob_offset_x"					"400"

	"knockback"						"10"
	"push"							"2200"
	"kick_time"						"400"
	"kick_amplitude"				"1"
	"dv_time"						"100"
	"gib"							"0"

	"default_surface_inanimate"		"wood"
	"default_surface_actor"			"flesh"

	"snd_hit"						"blackjack_hit_wood"
	"snd_metal"						"blackjack_hit_helmet"
	"snd_ricochet"					"blackjack_hit_flesh"
	"snd_flesh"						"blackjack_hit_flesh"
	"snd_undeadflesh"				"blackjack_hit_flesh"
	"snd_stone"						"blackjack_hit_wood"
	"snd_wood"						"blackjack_hit_wood"
	"snd_cardboard"					"blackjack_hit_wood"
	"snd_glass"						"glass_bullethole"
	"snd_liquid"					"melee_hit_water"
	"snd_plastic"					"blackjack_hit_wood"
	"snd_armor_plate"				"blackjack_hit_helmet"
	"snd_armor_chain"				"blackjack_hit_flesh"
	"snd_armor_leath"				"blackjack_hit_flesh"
	"snd_cloth"						"blackjack_hit_flesh"

	"sprS_metal"					"blackjack_hit_hard"
	"sprS_ricochet"					"blackjack_hit_hard"
	"sprS_stone"					"blackjack_hit_hard"
	"sprS_wood"						"blackjack_hit_hard"
	"sprS_glass"					"blackjack_hit_hard"
}

entityDef melee_policebaton_overhead 
{
	"inherit"						"melee_policebaton"
	"kickDir"						"-1 0 0"
}

entityDef atdmu:moveable_weapon_policebaton
{
	"inherit"						"atdmu:moveable_weapon_melee"
	"model"							"models/md5/weapons/policebaton/policebaton.lwo"

	"mass"							"6"
}

entityDef atdmu:weapon_policebaton
{
	"inherit"						"atdm:weapon_base"

	"editor_usage"					"The Police Baton"
	"editor_displayFolder"			"Weapons/Melee"
	
	"inv_weapon_name"				"policebaton" 
	"inv_name"						"Police Baton"
	"inv_icon"						"tdmu_hud_icon_weapon_policebaton"

	"spawnclass"					"idMoveable"
	"model"				 			"models/md5/weapons/policebaton/policebaton.lwo"
	"model_view" 					"viewmodel_policebaton"

	"weapon_scriptobject"			"weapon_blackjack"

	"frobable"						"1"
	"is_weapon_melee"				"1"
	"inv_lgmodifier"				"1"
	"ammo_required"					"0"
	"impact_damage_effect"			"1"

	"def_melee"						"melee_policebaton_overhead"
	"def_attach"					"atdmu:attachment_policebaton"
	"name_attach"					"meleeweap_r"

	"snd_acquire"					"tool_pickup"
	"snd_swing"						"blackjack_swing"

	"anim_rate_hit"					"2.0"
}

entityDef atdmu:attachment_policebaton
{
	"inherit"						"atdmu:moveable_weapon_policebaton"
	"spawnclass"					"CMeleeWeapon"
	"joint"							"Melee"

	"angles"						"0 0 0"
	"origin"						"0 0 0"

	"hide"							"1"
	"solid"							"0"

	"clipmodel_contents"			"0"
	"frobable"						"0"
	"neverShow"						"1"
	"noshadows"						"1"

	"use_larger_ai_head_CMs"		"1"

	"att_type_overhead" 			"0"
	"att_world_collide_overhead"	"1"
	"def_damage_overhead"			"melee_policebaton_overhead"
	"att_mod_cm_overhead"			"1"
	"att_cm_maxs_overhead"			"2.5 30 5"
	"att_cm_mins_overhead"			"-2.5 5 -5"

	"att_failsafe_trace_overhead"		"1"
	"att_failsafe_trace_start_overhead"	"-15 25 0"
	"att_failsafe_trace_end_overhead"	"10 25 0"
}
