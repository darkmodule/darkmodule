/***********************************************************************
This entityDef contains the shock rifle.
***********************************************************************/

model viewmodel_rifle_shock
{
	mesh							models/md5/weapons/rifle/rifle_shock.md5mesh
	offset							(2 0 -16)

	anim raise 						models/md5/weapons/rifle/select.md5anim
	{
		frame 15					sound_weapon rifle_raise
	}
	anim idle 						models/md5/weapons/rifle/idle.md5anim
	anim lower	 					models/md5/weapons/rifle/holster.md5anim
	{
		frame 5						sound_weapon rifle_lower
	}
	anim attack_start				models/md5/weapons/rifle/draw.md5anim
	{
		frame 30					sound_weapon rifle_charge_normal // 2 attached parts
	}
	anim attack 					models/md5/weapons/rifle/fire.md5anim
	{
		frame 1						sound rifle_fire_shock
	}
	anim tired		 				models/md5/weapons/rifle/tired.md5anim
	anim cancel		 				models/md5/weapons/rifle/cancel.md5anim
}

entityDef atdmu:weapon_rifle_shock
{
	"inherit"						"atdmu:weapon_rifle"

	"editor_usage"					"The Shock Rifle"

	"spawnclass"					"idMoveable"
	"model"							"models/md5/weapons/rifle/rifle_shock.lwo"
	"model_view"					"viewmodel_rifle_shock"

	"def_projectile"				"atdmu:projectile_plasma_shock"
	"def_ammo"						"atdmu:ammo_cell_shock"
	"def_attach"					"atdmu:attachment_plasma_shock"

	"inv_name"						"Shock Rifle"
	"inv_icon"						"tdmu_hud_icon_weapon_rifle_shock"
	"inv_weapon_name"				"rifle_shock" // Corresponding ammo

	"anim_rate_raise"				"1.5"
	"anim_rate_idle"				"0.5"
	"anim_rate_lower"				"1.5"
	"anim_rate_attack_start"		"0.75"
	"anim_rate_attack"				"1.0"
	"anim_rate_tired"				"1.0"
	"anim_rate_cancel"				"1.0"
}

entityDef atdmu:ammo_cell_shock
{
	"inherit"						"atdm:ammo_arrow"

	"editor_usage"					"Shock Rifle Ammunition"
	"editor_displayFolder"			"Weapons/Rifles"

	"model"							"models/md5/weapons/rifle/cell_shock.lwo"

	"inv_name"						"Shock Rifle Cell"
	"inv_weapon_name"				"rifle_shock" // Corresponding weapon
	"inv_ammo_amount"				"1" // Amount of ammo received upon pickup

	"clipmodel_contents"			"1056" // MOVEABLE_CLIP|CORPSE
	"removeWithMaster"				"0"
}

entityDef atdmu:attachment_plasma_shock
{
	"inherit"						"atdmu:attach_plasma"
}

entityDef atdmu:projectile_plasma_shock
{
	"inherit"						"atdmu:projectile_plasma"

	"model"							"models/md5/weapons/rifle/plasma_shock.lwo"

	"has_result"					"1"
	"def_result"					"atdmu:result_plasma_shock"
	"def_damage"					"atdm:damage_none"
}

entityDef atdmu:result_plasma_shock
{
	"inherit"						"atdmu:result_plasma"

	"scriptobject"					"result_flashbomb"

	"def_flashlight"				"atdmu:result_plasma_shock_light"

	"model_dud"						"flashbomb.prt"
	"snd_dud"						"tdm_flashbomb"

	//"gui"							"guis/playertools/flashbomb.gui"

	"copy_bind"						"0"

	"remove_delay"					"5"
	
	"sr_type_1"						"24" // STIM_FLASH
	"sr_radius_1"					"500"
	"sr_falloffexponent_1"			"1"
	"sr_duration_1"					"100" // ms
	"sr_time_interval_1"			"120" // ms
}

entityDef atdmu:result_plasma_shock_light
{
	"inherit"						"atdm:light_base"
	"light_radius"					"800 800 800"
	"texture"						"lights/brightround"
	"start_off"						"1"
}
