#!/bin/bash
# packs each pk4dir directory into a pk4 archive

for dir in *.pk4dir; do
	cd "${dir}"

	dir_noext=$(basename "${dir}" ".pk4dir")
	zip -r -u "../../${dir_noext}.pk4" *

	cd ".."
done
