This file contains the credits for The Dark Module.

1.1. Models, Characters:

- Nifrek (GPL): Megaerebus, Erebus, Nyx, Ignis, Seraphina, Pyria, Umbra, Gak.
- Nifrek, MirceaKitsune (GPL): Terminus, Termina, Draconi, Draconia.

1.2. Models, Weapons:

- MirceaKitsune (CC0): Police Baton, Crowbar.
- Duion (CC0): Baseball Bat.
- EdwardStevenson (CC0): Combat Knife.
- ScoutingNinja (CC0): Compound Bow.
- Diabolik (GPL): Rifle.
- Morphed (GPL): Cell.
- MirceaKitsune (CC0): Plasma.

2.1. Textures, Map:

- Georges 'TRaK' Grondin (GPL): trak4, trak5, trak6, trak7.
- Evillair (GPL): ex, ex2.
- Philip Klevestav (GPL): pk01, pk02.
- Oblivion (GPL): facility114, facility114inv.
- Narvik86, Morphed (GPL): narmor.
- TZork (GPL): terrain01.

2.2. Textures, GFX:

- Sev (GPL): HUD.

3.1. Sounds, Weapons:

- Michel Baradari (CC-BY): Rifle.
