// humans, ignis

model troopers_ignis_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/ignis_head.md5mesh
}

model troopers_ignis_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/ignis_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_ignis_head
{
	"editor_usage"					"Ignis head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_ignis_head"
	"skin"							"troopers/ignis_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_ignis
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Ignis."
	"editor_displayFolder"			"AI/Troopers/Humans"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_ignis_body"
	"skin"				 			"troopers/ignis_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_ignis_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"100"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_cynic_citywatch"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"0"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_ignis
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Ignis."

	"model"							"troopers_ignis_body"
	"skin"							"troopers/ignis_body"
	"def_head"						"atdm:ai_troopers_ignis_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}

// humans, ignis masked

model troopers_ignis_masked_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/ignis_masked_head.md5mesh
}

entityDef atdm:ai_troopers_ignis_masked_head
{
	"editor_usage"					"Ignis head, masked."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_ignis_masked_head"
	"skin"							"troopers/ignis_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_ignis_masked
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Ignis, masked."
	"editor_displayFolder"			"AI/Troopers/Humans"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_ignis_body"
	"skin"				 			"troopers/ignis_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_ignis_masked_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_MALE"

	"health"						"100"
	"mass"							"100"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_cynic_citywatch"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_ignis_masked
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Ignis, masked."

	"model"							"troopers_ignis_body"
	"skin"							"troopers/ignis_body"
	"def_head"						"atdm:ai_troopers_ignis_masked_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_MALE"
}

// humans, seraphina

model troopers_seraphina_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/seraphina_head.md5mesh
}

model troopers_seraphina_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/seraphina_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_seraphina_head
{
	"editor_usage"					"Seraphina head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_seraphina_head"
	"skin"							"troopers/seraphina_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_seraphina
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Seraphina."
	"editor_displayFolder"			"AI/Troopers/Humans"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_seraphina_body"
	"skin"				 			"troopers/ignis_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_seraphina_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_FEMALE"

	"health"						"100"
	"mass"							"100"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_wench_soft_guard_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"0"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_seraphina
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Seraphina."

	"model"							"troopers_seraphina_body"
	"skin"				 			"troopers/ignis_body"
	"def_head"						"atdm:ai_troopers_seraphina_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_FEMALE"
}

// humans, seraphina masked

model troopers_seraphina_masked_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/seraphina_masked_head.md5mesh
}

entityDef atdm:ai_troopers_seraphina_masked_head
{
	"editor_usage"					"Seraphina head, masked."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_seraphina_masked_head"
	"skin"							"troopers/ignis_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_seraphina_masked
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Seraphina, masked."
	"editor_displayFolder"			"AI/Troopers/Humans"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_seraphina_body"
	"skin"				 			"troopers/ignis_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_seraphina_masked_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_FEMALE"

	"health"						"100"
	"mass"							"100"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_wench_soft_guard_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_seraphina_masked
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Seraphina, masked."

	"model"							"troopers_seraphina_body"
	"skin"				 			"troopers/ignis_body"
	"def_head"						"atdm:ai_troopers_seraphina_masked_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_FEMALE"
}

// humans, pyria

model troopers_pyria_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/pyria_head.md5mesh
}

model troopers_pyria_body
{
	inherit							tdm_ai_proguard
	mesh							models/md5/chars/troopers/pyria_body.md5mesh
	channel torso					(*Spine_Dummy)
	channel legs					(origin Pelvis Pelvis2 *Hips)
}

entityDef atdm:ai_troopers_pyria_head
{
	"editor_usage"					"Pyria head."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_pyria_head"
	"skin"							"troopers/pyria_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_pyria
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Pyria."
	"editor_displayFolder"			"AI/Troopers/Humans"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_pyria_body"
	"skin"				 			"troopers/pyria_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_pyria_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_FEMALE"

	"health"						"100"
	"mass"							"100"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_wench_soft_guard_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"0"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_pyria
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Pyria."

	"model"							"troopers_pyria_body"
	"skin"							"troopers/pyria_body"
	"def_head"						"atdm:ai_troopers_pyria_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_FEMALE"
}

// humans, pyria masked

model troopers_pyria_masked_head
{
	inherit							head_model_base
	mesh							models/md5/chars/troopers/pyria_masked_head.md5mesh
}

entityDef atdm:ai_troopers_pyria_masked_head
{
	"editor_usage"					"Pyria head, masked."
	"inherit"						"atdm:ai_head_base"
	"model"							"troopers_pyria_masked_head"
	"skin"							"troopers/pyria_masked_head"
	"_color"						"1 1 1" // shirt and glow color

	"ko_alert_immune_state"			"5" // cannot be KO'd when in Combat
}

entityDef atdm:ai_troopers_pyria_masked
{
	"inherit"						"atdm:ai_humanoid_newskel"
	"editor_usage"					"Pyria, masked."
	"editor_displayFolder"			"AI/Troopers/Humans"

	"ragdoll"						"guard_base_newskel"
	"model"							"troopers_pyria_body"
	"skin"				 			"troopers/pyria_masked_body"
	"_color"						"1 1 1" // shirt and glow color
	"def_head"						"atdm:ai_troopers_pyria_masked_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_CITYWATCH"
	"personGender"					"PERSONGENDER_FEMALE"

	"health"						"100"
	"mass"							"100"

	"team"							"2"
	"rank"							"2"

	"is_civilian"					"0"
	"canLightTorches"				"1"
	"canOperateSwitchLights"		"1"

	"def_vocal_set"					"atdm:ai_vocal_set_wench_soft_guard_01"
	"def_melee_set"					"atdm:ai_melee_set_skilled"
	"draws_weapon"					"1"

	"ko_alert_immune"				"1"

	"hide_distance"					"-1" // never
	"dist_check_period"				"0.7"
}

entityDef atdm:env_ragdoll_troopers_pyria_masked
{
	"inherit"						"atdm:env_ragdoll_humanoid_base_newskel"
	"editor_usage"					"Ragdoll for Pyria, masked."

	"model"							"troopers_pyria_body"
	"skin"							"troopers/pyria_masked_body"
	"def_head"						"atdm:ai_troopers_pyria_masked_head"
	"offsetHeadModel"				"1.0 0 -2.8"

	"AIUse"							"AIUSE_PERSON"
	"personType"					"PERSONTYPE_GENERIC"
	"personGender"					"PERSONGENDER_FEMALE"
}
